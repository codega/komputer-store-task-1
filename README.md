# Komputer Store

Assignment 1 for Experis Academy's JavaScript course. A 'cookie-clicker'-esque website where you can work and loan money to 'buy' a computer.

## Getting Started
### Prerequesites

* Visual Studio Code 
* Live Server for VSCode

### Open the webpage
Open index.html with Live Server 
> Alt + L Alt + O

## Built With

* Visual Studio Code
* Live Server for VSCode
* Bootstrap

## Resources

I stole all laptop-image links and laptop-descriptions/features from elkjop.no

## Author

* Charlotte Ødegården

## License

I don't know enough about licenses to firmly state anything about this. 