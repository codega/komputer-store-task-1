//Dom Elements
//Buttons
const elLoanBtn = document.getElementById('loan');
const elWorkBtn = document.getElementById('work');
const elTransferBtn = document.getElementById('transfer');
const elBuyBtn = document.getElementById('buyLaptop');
//Money
const elPay = document.getElementById('pay');
const elBalance = document.getElementById('balance');
//Laptop
const elLaptops = document.getElementById('laptops');
const elFeatures = document.getElementById('features');
const elLaptopImg = document.getElementById('laptopImg');
const elLaptopPrice = document.getElementById('laptopPrice');
const elLaptopDescription = document.getElementById('laptopDescription');
const elLaptopTitle = document.getElementById('laptopTitle');
//Display
const elComputerDisplay = document.getElementById('computerDisplay');
const elCustomerName = document.getElementById('customerName');

//Laptop-descriptions
const macbookDescription = 'Den nye MacBook Air 2020 med sitt svært lettvektige aluminiumskabinett er den ideelle hverdagsmaskinen. Den er utstyrt med Retina-skjerm og en 10. generasjons Intel Core-prosessor for produktivitet, ytelse og underholdning både hjemme og på farten.';
const hpDescription = "Denne bærbare PC-en fra HP har et stilig og lett design på 1,5 kg, samt en effektiv prosessor som gir lang batteritid. Med det kan du komfortabelt jobbe på farten eller se favorittseriene dine på en 14 inch FHD-skjerm med lyd fra doble høyttalere.";
const asusDescription = 'Den bærbare PC-en Asus Laptop 14 er en strømeffektiv maskin for hverdagslige oppgaver som skriving og surfing på nettet. Den er svært slank og lettvektig, så det er lett å ta den med for å jobbe eller surfe på nettet når du er på farten.';
const samsungDescription = 'Samsung Galaxy Book ION bærbar PC kombinerer en svært lett vekt og portabilitet med en høytytende brikke som leverer avansert ytelse under krevende oppgaver. QLED-skjermen byr på presis fargegjengivelse slik at du kan jobbe hvor som helst.';

//Using links for images in stead of files would be a bad idea for an actual website - links can break.
const laptop1 = {name:'Macbook Air 2020', price: 6000, image:'https://www.elkjop.no/image/dv_web_D180001002461047/161636/macbook-air-2020-133-256-gb-stellargraa.jpgbookAir.jpg', description: macbookDescription, features: ['10. gen. Intel® Core™ i313', '256 GB SSD', '8 GB RAM']};
const laptop2 = {name:'HP 14 DQ0003', price: 1000, image:'https://www.elkjop.no/image/dv_web_D180001002464638/179356/hp-14-dq0003-14-baerbar-pc-hvit.jpg', description: hpDescription, features: ['Intel® Celeron™ N4000', '4 GB DDR4 RAM', '64 GB eMMC flashminne']}; 
const laptop3 = {name:'ASUS Laptop 14', price: 3000, image:'https://www.elkjop.no/image/dv_web_D180001002466823/81927/asus-laptop-14-baerbar-pc.jpg', description: asusDescription, features: ['Intel® Celeron® N4000', '4 GB DDR4 RAM', '64 GB eMMC-lagring']};
const laptop4 = {name:'Samsung Galaxy Book ION', price: 9000, image:'https://www.elkjop.no/image/dv_web_D180001002468136/181551/samsung-galaxy-book-ion-133-baerbar-pc-aura-silver.jpg', description: samsungDescription, features:['Intel® Core™ i7-10510U', '512 GB PCIe SSD', '16 GB DDR4 RAM']};
const laptops = [laptop1, laptop2, laptop3, laptop4];

let bankBalance = 0;
let workBalance = 0;
const pay = 100;
let loan = false;
let currentLaptop;

//Asking users for the name on 'setup' - a really annoying way (for users) to do that
const customerName = prompt("What's your name?", "Enter name here");
elCustomerName.innerText = customerName;

function renderMoney(){
    elPay.innerHTML = workBalance;
    elBalance.innerHTML = bankBalance;
}

function renderComputer(chosenLaptop){
    let index = parseInt(chosenLaptop.value);
    currentLaptop = laptops[index];

    elLaptopImg.src = currentLaptop.image;
    elLaptopPrice.innerText = currentLaptop.price;
    elLaptopDescription.innerText = currentLaptop.description;
    elLaptopTitle.innerText = currentLaptop.name;
    elFeatures.innerText = '';
    currentLaptop.features.forEach((feature)=> {
        const elFeature = document.createElement('li');
        elFeature.innerText = feature;
        elFeatures.appendChild(elFeature);
    });

    elComputerDisplay.removeAttribute("hidden");
}

const newPay = (work) => work + pay;

const transfer = function(){
    bankBalance += workBalance;
    workBalance = 0;
    renderMoney();
}

const buy = function(){
    if (currentLaptop.price > bankBalance){
        alert("You can't afford this one!");
    } else {
        bankBalance -= currentLaptop.price;
        renderMoney();
        alert("You've bought a computer!");
    }
}

const getALoan = function(){
    if (loan){
        alert("You already have a loan!");
    } else {
        let input = prompt("Please enter loan amount", "Enter here"); 
        let requestedLoanAmount = parseInt(input);
        if (requestedLoanAmount <= bankBalance * 2){
            loan = true;
            bankBalance+= requestedLoanAmount;
            renderMoney();
        }
        else {
            alert("You don't have enough money to loan that much!");
        }
    }
}

elWorkBtn.addEventListener('click', function(event){
    workBalance = newPay(workBalance);
    renderMoney();
})

elTransferBtn.addEventListener('click', transfer);

elLoanBtn.addEventListener('click', getALoan);

elBuyBtn.addEventListener('click', buy);